const tf = require("@tensorflow/tfjs-node");

async function init() {
  const model = await tf.loadLayersModel("file://model-js/model.json");
  for (let i = 0; i < 15; i++) {
    const arr = model.predict(tf.tensor2d([[i, 0]])).dataSync();
    console.log(arr[0]);
  }
}

init();
