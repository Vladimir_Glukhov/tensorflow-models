const tf = require("@tensorflow/tfjs-node");
const { x_train, y_train } = require("./data");

const model = tf.sequential();
model.add(tf.layers.dense({ inputShape: [2], units: 32, activation: "relu" }));
model.add(tf.layers.dense({ units: 32, activation: "relu6" }));
model.add(tf.layers.dense({ units: 32, activation: "relu6" }));
model.add(tf.layers.dense({ units: 4, activation: "relu" }));

model.summary();
model.compile({
  optimizer: tf.train.sgd(0.0001),
  loss: "meanSquaredError",
});

model
  .fit(tf.tensor2d(x_train), tf.tensor2d(y_train), {
    epochs: 300,
    batchSize: 4,
    callbacks: {
      onEpochEnd(e, l) {
        console.log(e, l);
      },
    },
  })
  .then(() => {
    model.predict(tf.tensor2d([[0, 0]])).print();
    model.predict(tf.tensor2d([[5, 0]])).print();
    model.predict(tf.tensor2d([[10, 0]])).print();

    model.predict(tf.tensor2d([[0, 0]])).print();
    model.predict(tf.tensor2d([[-5, 0]])).print();
    model.predict(tf.tensor2d([[-10, 0]])).print();

    console.log(model.predict(tf.tensor2d([[-10, 0]])).dataSync())
    model.save('file://model-js');
  });
