# tensorflow-models

run 'npm install'.

go to folder /models/position-resolver and run 'node loadmodel' to see how work this model.

also, you can re-train model with your data, just write it into 'data.js' file

# data format

input: [[x, y]] - x, y are angles

output: [[y0, y1, y2, y3]] - coordinates of y-axis of each robot's leg